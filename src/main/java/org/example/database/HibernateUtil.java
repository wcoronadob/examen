package org.example.database;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    static {
        try {
            // Crea la SessionFactory al iniciar la aplicación
            sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
            System.out.println("Hola, mundo!!!!!!!!!!!!");
        } catch (Throwable ex) {
            // Manejo de excepciones en caso de error al crear la SessionFactory
            System.err.println("Error al inicializar la SessionFactory: " + ex);
            System.out.println("Hola, mundo!");
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() {
        // Método para obtener una nueva sesión de Hibernate
        return sessionFactory.openSession();
    }

    public static void closeSession(Session session) {
        // Método para cerrar una sesión de Hibernate
        if (session != null && session.isOpen()) {
            session.close();
        }
    }

    public static void closeSessionFactory() {
        // Método para cerrar la SessionFactory al finalizar la aplicación
        if (sessionFactory != null && !sessionFactory.isClosed()) {
            sessionFactory.close();
        }
    }

}
