package org.example.negocios;
import java.util.List;

import org.example.database.HibernateUtil;
import org.example.modelos.Propiedades;
import org.hibernate.Session;
import org.hibernate.query.Query;


public class PropiedadesManager {

    public static void crearPropiedad(String nombre, String direccion /* otros parámetros */) {
        // Crear una instancia de Propiedades y configurar sus atributos
        Propiedades nuevaPropiedad = new Propiedades();
        nuevaPropiedad.setNombre(nombre);
        nuevaPropiedad.setDireccion(direccion);
        // Configurar otros atributos según sea necesario

        // Obtener una sesión de Hibernate
        Session session = HibernateUtil.getSession();

        try {
            // Iniciar una transacción
            session.beginTransaction();

            // Guardar el objeto en la base de datos
            session.save(nuevaPropiedad);

            // Confirmar la transacción
            session.getTransaction().commit();
        } catch (Exception e) {
            // Manejar excepciones
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            e.printStackTrace();
        } finally {
            // Cerrar la sesión de Hibernate
            HibernateUtil.closeSession(session);
        }
    }

    // Método para obtener la lista de todas las propiedades
    public static List<Propiedades> listarPropiedades() {
        // Obtener una sesión de Hibernate
        Session session = HibernateUtil.getSession();

        try {
            // Iniciar una transacción
            session.beginTransaction();

            // Realizar una consulta HQL para obtener todas las propiedades
            Query<Propiedades> query = session.createQuery("FROM Propiedades", Propiedades.class);
            List<Propiedades> propiedades = query.getResultList();

            // Confirmar la transacción
            session.getTransaction().commit();

            // Devolver la lista de propiedades
            return propiedades;
        } catch (Exception e) {
            // Manejar excepciones
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            e.printStackTrace();
            return null;
        } finally {
            // Cerrar la sesión de Hibernate
            HibernateUtil.closeSession(session);
        }
    }
}







