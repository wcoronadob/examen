package org.example;
import org.example.database.HibernateUtil;
import org.hibernate.Session;


// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Session session = HibernateUtil.getSession();

        // Realiza operaciones con Hibernate usando la sesión

        HibernateUtil.closeSession(session);
        HibernateUtil.closeSessionFactory();
    }

}