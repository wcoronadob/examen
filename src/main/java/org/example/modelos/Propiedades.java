package org.example.modelos;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
public class Propiedades {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(length = 100)
    private String nombre;

    @Column(length = 200)
    private String direccion;

    @Column(length = 200)
    private String caracteristicas;

    @Column(length = 20)
    private String estado;

    @Column(precision = 10, scale = 2)
    private BigDecimal precioAlquiler;

    @Column
    private LocalDateTime create_at;

    @Column
    private LocalDateTime update_at;

    @PrePersist
    protected void onCreate() {
        create_at = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        update_at = LocalDateTime.now();
    }

}
